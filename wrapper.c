#define _GNU_SOURCE
#define _DEFAULT_SOURCE
#define _POSIX_C_SOURCE

#define CHECK_SERVER_GROUPS 1

#define FROSTY_USER "frosty"
#define FROSTY_SH "frosty.sh"

#define CBLUE "\033[1;34m"
#define CGREEN "\033[1;32m"
#define CCYAN "\033[1;36m"
#define CRED "\033[1;31m"
#define CPURPLE "\033[1;35m"
#define CYELLOW "\033[1;33m"
#define CWHITE "\033[1;37m"
#define CRESET "\033[0m"


#include <pwd.h>
#include <grp.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

    static const char FROSTY_SERVER_DIR[] = "/var/frosty/servers/";
    static const char FROSTY_ARCHIVE_DIR[] = "/var/frosty/archives/";
    static const char FROSTY_BACKUP_DIR[] = "/var/frosty/backup/";
    struct passwd *pwd;
    static char *envp[2];
    gid_t gid_to_use = 0;
    char **env;

    envp[(sizeof(envp) - 1) / sizeof(*envp)] = NULL;
    for (env = environ; *env; env++) {
        if (!strncmp(*env, "TERM=", 5)) {
            envp[0] = *env;
            break;
        }
    }

    if (getuid() == 0) {
        fprintf(stderr, CRED "error: frosty MUST NOT be run as root!!!" CRESET "\n");
        return EXIT_FAILURE;
    }

    // list needs multiple groups since we might have multiple servers
    // to query at once. So we don't bother even changing user
    // and just execute it directly.
    if (argc == 2 && strcmp(argv[1], "list") == 0) {
        // exec
        seteuid(getuid());
        execvpe(FROSTY_SH, argv, envp);
        perror("exec");
        return EXIT_FAILURE;
    }

    // Before doing any of the setuid/setgid stuff, we should figure out
    // which server the user is trying to access. If not accessing
    // any server, then we can let the user keep going through...
#if CHECK_SERVER_GROUPS
    if (argc >= 3) {
        char *server_name = argv[2];
        char *server_path, *archive_path, *backup_path;

        if (strcmp(argv[1], "help") != 0) {

            for (char *c = server_name; *c != '\0'; c++) {
                if (!isalnum(*c) && *c != '_') {
                    fprintf(stderr, CRED "error: illegal server name. only letters, numbers, and underscores are allowed!" CRESET "\n");
                    return EXIT_FAILURE;
                }
            }

            // Determine the executing user's groups. EUID is root right now, so we can't use that
            int ngroups = 16;
            gid_t *groups = (gid_t *) malloc(ngroups * sizeof(*groups));
            struct passwd *pw = getpwuid(getuid());
            if (pw == NULL) {
                fprintf(stderr, CRED "internal error: could not look up user!?" CRESET "\n");
                return EXIT_FAILURE;
            }

            if (getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups) < 0) {
                free(groups);
                gid_t *groups = (gid_t *) malloc(ngroups * sizeof(*groups));
                getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);
            }

            // Compose the actual path to the server, archives, and
            // backups
            if ((server_path = malloc(sizeof(FROSTY_SERVER_DIR) + strlen(server_name) + 1)) == NULL) {
                perror(CRED "internal error: malloc failed" CRESET);
                return EXIT_FAILURE;
            }
            if ((archive_path = malloc(sizeof(FROSTY_ARCHIVE_DIR) + strlen(server_name) + 1)) == NULL) {
                perror(CRED "internal error: malloc failed" CRESET);
                return EXIT_FAILURE;
            }
            if ((backup_path = malloc(sizeof(FROSTY_BACKUP_DIR) + strlen(server_name) + 1)) == NULL) {
                perror(CRED "internal error: malloc failed" CRESET);
                return EXIT_FAILURE;
            }

            // Do not copy ending null byte for FROSTY_SERVER_DIR
            strncpy(server_path, FROSTY_SERVER_DIR, sizeof(FROSTY_SERVER_DIR) - 1);
            strncpy(server_path + sizeof(FROSTY_SERVER_DIR) - 1, server_name, strlen(server_name) + 1);
            strncpy(archive_path, FROSTY_ARCHIVE_DIR, sizeof(FROSTY_ARCHIVE_DIR) - 1);
            strncpy(archive_path + sizeof(FROSTY_ARCHIVE_DIR) - 1, server_name, strlen(server_name) + 1);
            strncpy(backup_path, FROSTY_BACKUP_DIR, sizeof(FROSTY_BACKUP_DIR) - 1);
            strncpy(backup_path + sizeof(FROSTY_BACKUP_DIR) - 1, server_name, strlen(server_name) + 1);

            if (strcmp(argv[1], "create") == 0) {
                // We are running the "create" command, so group checking is entirely irrelevant.
                // Need to prompt the user for which group they want to use to make it.
                printf(CGREEN "which group should own this server?" CRESET "\n");

                for (int i = 0; i < ngroups; i++) {
                    struct group *gr = getgrgid(groups[i]);
                    printf(CBLUE "%d\t" CPURPLE "%s\n", i+1, gr->gr_name);
                }

                printf(CGREEN "type the number next to the group that you want, or Ctrl+C to cancel: " CBLUE);
                int selection;
                if (scanf("%d", &selection) != 1 || selection > ngroups || selection < 1) {
                    printf(CRED "error: invalid group selection. Try one of the numbers above next time." CRESET "\n");
                    return EXIT_FAILURE;
                }

                gid_to_use = groups[selection-1];
                struct group *gr = getgrgid(gid_to_use);
                printf(CGREEN "ok, using group " CPURPLE "%s (gid=%d)" CGREEN " for the new server!\n", gr->gr_name, gr->gr_gid);

                // We need to make the directory now, and set its permissions correctly.
                // We can't use chmod 077 with frosty as owner, since then frosty
                // can't access it (unix permissions are "explicitly deny" or "explicitly allow",
                // unfortunately)
                // For this reason, we will use root as the owner user of the directory, and
                // the correct group as the owner group.
                if (mkdir(server_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to make directory for the new server" CRESET);
                    return EXIT_FAILURE;
                }

                if (chown(server_path, 0, gid_to_use)) {
                    perror(CRED "error: failed to change ownership of new server directory" CRESET);
                    return EXIT_FAILURE;
                }

                if (chmod(server_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to change permissions of new server directory" CRESET);
                    return EXIT_FAILURE;
                }

                // ditto, for archive dir
                if (mkdir(archive_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to make archive directory for the new server" CRESET);
                    return EXIT_FAILURE;
                }

                if (chown(archive_path, 0, gid_to_use)) {
                    perror(CRED "error: failed to change ownership of new server archive directory" CRESET);
                    return EXIT_FAILURE;
                }

                if (chmod(archive_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to change permissions of new server archive directory" CRESET);
                    return EXIT_FAILURE;
                }

                // ditto, for backup dir
                if (mkdir(backup_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to make backup directory for the new server" CRESET);
                    return EXIT_FAILURE;
                }

                if (chown(backup_path, 0, gid_to_use)) {
                    perror(CRED "error: failed to change ownership of new server backup directory" CRESET);
                    return EXIT_FAILURE;
                }

                if (chmod(backup_path, S_IRWXG | S_ISGID)) {
                    perror(CRED "error: failed to change permissions of new server backup directory" CRESET);
                    return EXIT_FAILURE;
                }

            } else {
                // Managing some existing server
                struct stat statbuf;
                if (stat(server_path, &statbuf)) {
                    perror(CRED "error: are you trying to access a server that does not exist?\ncould not check permissions for server..." CRESET);
                    return EXIT_FAILURE;
                }

                // Let's now check if the user has this group at all...
                // if the correct group was found, save it for later, so we can use it
                int group_found = 0;
                for (int i = 0; i < ngroups; i++) {
                    if (groups[i] == statbuf.st_gid) {
                        group_found = 1;
                        gid_to_use = statbuf.st_gid;
                    }
                }

                if (!group_found) {
                    fprintf(stderr, CRED "error: you are not authorized to access server " CCYAN "%s" CRED "!\n", server_name);
                    return EXIT_FAILURE;
                }

            }
            free(server_path);
        }
    }
#endif /* check server groups */

    // we should 


    if ((pwd = getpwnam(FROSTY_USER)) == NULL) {
        perror("user lookup");
        return EXIT_FAILURE;
    }

    // use frosty's gid if auth was OK but no groups match
    if (gid_to_use == 0) gid_to_use = pwd->pw_gid;

    if (setgid(gid_to_use)) {
        perror("setgid");
        return EXIT_FAILURE;
    }

    if (setgroups(1, &gid_to_use)) {
        perror("setgroups");
        return EXIT_FAILURE;
    }

    // setuid to frosty
    if (setuid(pwd->pw_uid)) {
        perror("setuid");
        puts("hint: did you forget to execute this program as setuid root?");
        return EXIT_FAILURE;
    }

    // exec
    execvpe(FROSTY_SH, argv, envp);
    perror("exec");
    return EXIT_FAILURE;

}

