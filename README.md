# frosty

Do you want a webUI-free administration experience for your game servers?
Do you run your servers on Linux, and want a simple solution?

Then try frosty, a cool helper for some of your server management needs!

WARNING: only run this on a machine where you trust all the users.
It's still not fully fleshed out and there may be nasty bugs.

## Features
- Lean and fast
- Differential backups with `rdiff-backup`
- Multiple servers
- Proper, direct console access through `tmux`
- Adaptable to different game servers

## Getting started

- Make sure you are running Linux (it might work with Mac or BSD, but I have
  not tested it.)

### Install dependencies

```
# apt-get install tmux rdiff-backup
```

### Create frosty user

This command is Debian/*buntu specific. Consult your Linux distribution's
documentation on how to create a user if you are not running a Debian-based
distribution. You will need a frosty user and group. The shell, home directory,
password, and other options don't matter.

```
# adduser --system --no-create-home --group --disabled-password --disabled-login frosty
```

### Install into `/usr/local/bin` and create socket directories:

```
# ./setup.sh
```

### Create your first server

```
$ frosty create foo
server 'foo' sucessfully created
edit '/var/frosty/servers/foo/start.sh' to define what this runs
```

### Configure your first server

```
$ cat > /var/frosty/servers/foo/start.sh <<EOF
#!/bin/bash
java -jar minecraft_server.jar nogui
EOF
$ echo "stop" > /var/frosty/servers/foo/stop_command
```

### Start your first server

```
$ frosty start foo
```

## Usage

`frosty COMMAND [ARGS...]`

Run `frosty help COMMAND` for more help about a particular command.

```
usage: frosty <command> [arguments...]

commands:
help <command> - print this usage or detailed help for the given command
list - list all servers frosty manages
create <name> - create a server
remove <name> - remove (delete) a server
start <name> - start a server
stop <name> - stop a server
restart <name> - restart a server
status <name> - print status of a server
kill [args] <name> - send a signal to a server
backup <name> [create|restore <how-long-ago>|remove <how-long-ago>|list] - differential backup
archive <name> [create|list] - full archive
console <name> [command...] - open the console or send a command
path <name> - print the server's directory name
fix <name> - fix permissions for files in the server
```

## Architecture
When you call `frosty`, these are the things that happen
- `frosty` is setuid root. It changes user and group to `frosty`
  and clears the environment.
- `frosty` then executes `frosty.sh`, which manages the servers.
- Servers are wrapped in `tmux` and all run under the `frosty` user.
