#!/bin/bash -l 
. "/etc/frosty.conf" || exit 1

export SHELL=/bin/bash

frosty_version="0.1.0"
frosty_authors="Lekro"


blue="\033[1;34m"
green="\033[1;32m"
cyan="\033[1;36m"
red="\033[1;31m"
purple="\033[1;35m"
yellow="\033[1;33m"
white="\033[1;37m"
reset="\033[0m"

synopsis() {
    printf "usage: ${cyan}frosty ${green}<command> ${blue}[arguments...]${reset}\n"
}

usage() {
    if [ $# -eq 0 ]; then
        printf "${cyan}frosty${reset} version ${blue}${frosty_version}${reset} by ${green}${frosty_authors}${reset}\n"
        cmds="help list create remove start stop restart status kill backup"
        cmds="$cmds archive console path fix"
        synopsis
        echo
        echo "commands:"
    else
        cmds="$1"
    fi

    for cmd in $cmds; do
        command_usage "$cmd" || exit 1
    done
}

command_usage() {
    case "$1" in
        help)
            printf "${green}$1${reset} ${blue}<command>${reset} - print this usage or detailed help for the given command\n"
            ;;
        list|ls)
            printf "${green}$1${reset} - list all servers frosty manages\n"
            ;;
        create)
            printf "${green}$1${reset} ${blue}<name>${reset} - create a server\n"
            ;;
        remove|rm)
            printf "${green}$1${reset} ${blue}<name>${reset} - remove (delete) a server\n"
            ;;
        start)
            printf "${green}$1${reset} ${blue}<name>${reset} - start a server\n"
            ;;
        stop)
            printf "${green}$1${reset} ${blue}<name>${reset} - stop a server\n"
            ;;
        restart)
            printf "${green}$1${reset} ${blue}<name>${reset} - restart a server\n"
            ;;
        status)
            printf "${green}$1${reset} ${blue}<name>${reset} - print status of a server\n"
            ;;
        kill)
            printf "${green}$1${reset} ${blue}<name> [kill-args...]${reset} - send a signal to a server\n"
            ;;
        backup)
            printf "${green}$1${reset} ${blue}<name> [create|restore <how-long-ago>|remove <how-long-ago>|list]${reset} - differential backup\n"
            ;;
        archive)
            printf "${green}$1${reset} ${blue}<name> [create [prefix]|list|restore|purge <prefix> <how-long-ago-in-days>]${reset} - full archive\n"
            ;;
        console)
            printf "${green}$1${reset} ${blue}<name> [command...]${reset} - open the console or send a command\n"
            ;;
        path)
            printf "${green}$1${reset} ${blue}<name>${reset} - print the server's directory name\n"
            ;;
        fix)
            printf "${green}$1${reset} ${blue}<name>${reset} - fix permissions for files in the server\n"
            ;;
        *)
            printf "${red}error: unknown command${reset}\n" && return 1
            ;;
    esac
}

check_setup() {
    [ -d "$frosty_dir" ] || return 1
    [ -d "$server_dir" ] || return 1
    [ -d "$archive_dir" ] || return 1
    [ -d "$backup_dir" ] || return 1
}

fix_setup() {
    mkdir -p "$frosty_dir" "$server_dir" "$archive_dir" "$backup_dir"
    chgrp "$group" "$frosty_dir" "$server_dir" "$archive_dir" "$backup_dir"
    chmod g+w,o-rx "$frosty_dir" "$server_dir" "$archive_dir" "$backup_dir"
}

server_dir() {
    echo "$server_dir/$1"
}

server_status() {
    ls "$server_dir/$1" >/dev/null 2>&1 || return 2
    tmux -S "$sock" list-sessions -F "#S" 2>/dev/null | grep -q "$1" && return 0
    # [ -S "$frosty_dir/$1.sock" ] && return 0
    return 1
}

server_tmux() {
    tmux -S "$sock" source-file /var/frosty/tmux.conf \; $@
}

server_command() {
    scmd="$@"
    server_tmux send-keys "${scmd// / Space }" ENTER
}

check_server() {
    [ -d "$(server_dir $1)" ] && return 0
    return 1
}

[ $# -eq 0 ] && usage && exit 1
check_setup || { printf "${yellow}error: improper setup; fixing...${reset}\n" && fix_setup; }

cmd="$1"
shift

# Deal with commands not requiring a server.
case "$cmd" in
    --help|-h|help)
        [ $# -eq 0 ] && usage && exit 0
        usage "$1" && exit 0
        ;;
    list|ls)
        i=0
        for f in "$server_dir/"*; do
            if [ -d "$f" ]; then
                name="$(basename $f)"
                printf "${cyan}$name${reset} - $($0 status $name)\n"
            else
                continue
            fi
            i=$(expr "$i" + 1)
        done
        [ $i -eq  0 ] && printf "${cyan}frosty ${reset}isn't managing any servers yet! Try ${cyan}frosty ${green}create${reset}!\n"
        exit 0
        ;;
    create|remove|start|stop|restart|status|backup|archive|console|path|fix|kill)
        ;;
    *)
        printf "${red}error: unknown command${reset}\n"
        exit 1
        ;;
esac

# Get server name
[ $# -eq 0 ] && printf "${red}error: this command needs you to tell me what server to operate on${reset}\n" && usage "$cmd" && exit 1
name="$1"
shift

if false; then
    # commands requiring the server not to exist
    check_server "$name" && printf "${red}error: that server already exists!${reset}\n" && exit 1
    sock="/dev/null"
else
    # commands requiring the server to exist
    check_server "$name" || { printf "${red}error: that server doesn't exist!${reset}\n" && exit 1; }
    sock="$server_dir/$name/tmux.socket"
fi

case "$cmd" in
    create|remove|start)
        # commands requiring the server to be stopped
        server_status "$name" && printf "${red}error: server must not be running${reset}\n" && exit 1
        ;;
    stop|restart|console)
        # commands requiring the server to be started
        server_status "$name" || { printf "${red}error: server not started${reset}\n" && exit 1; }
        ;;
    *)
        ;;
esac

# default subcommands
case "$cmd" in
    backup|archive)
        if [ $# -ge 2 ]; then
            scmd="$1"
            sprefix="$2"
            shift; shift
        elif [ $# -eq 1 ]; then
            scmd="$1"
            sprefix="$cmd"
            shift
        else
            scmd="create"
        fi
        ;;
esac


sdir="$(server_dir $name)"

# deal with all commands requiring a server
case "$cmd" in
    create)
        # handled in the wrapper program since it sets permissions
        printf "${green}server ${cyan}$name${green} successfully created!${reset}\n"
        printf "(edit ${blue}$sdir/start.sh${reset} to define what this runs)\n"
        ;;
    remove)
        printf "${yellow}This command has been disabled by the server admin. Please contact a server admin for help.\n"
        exit 1
        printf "${yellow}PERMANENTLY delete server ${cyan}$name${yellow}? Type ${green}y${yellow} for yes or ${red}n${yellow} for no.${reset} "
        read yn
        case "$yn" in
            y)
                printf "${yellow}deleting ${cyan}${name}${yellow}... "
                rm -rf "$sdir"
                printf "${yellow}done deleting${reset}\n"
                ;;
            *)
                printf "${white}server not deleted${reset}\n"
                exit 1
                ;;
        esac
        exit 0
        ;;
    start)
        if [ -f "$sdir/frosty.pid" ]; then
            printf "${yellow}warning: PID file exists for server ${cyan}$name${yellow}. This may mean the server did not stop gracefully!\n"
            printf "${yellow}warning: Removing existing PID file...\n"
            rm -f "$sdir/frosty.pid"
        fi

        start_script="$sdir/start.sh"
        [ -f "$start_script" ] || { printf "${red}error: no start script at ${blue}$start_script${reset}\n" && exit 1; }
        [ -r "$start_script" ] || { printf "${red}error: unreadable start script at ${blue}$start_script${reset}\n" && exit 1; }
        [ -x "$start_script" ] || { printf "${red}error: unexecutable start script at ${blue}$start_script${reset}\n" && exit 1; }
        printf "${green}starting ${cyan}${name}${green}...${reset}\n"
        ( cd "$sdir" && server_tmux new-session -d -s "$name" "./start.sh" )
        ;;
    stop)
        stop_command="$sdir/stop_command"
        [ -f "$stop_command" ] || { printf "${red}error: no stop command at ${blue}$stop_command${reset}\n" && exit 1; }
        printf "${green}gracefully stopping ${cyan}${name}${green}...${reset}\n"
        server_command "$(cat $stop_command)"
        ;;
    kill)
        if ! [ -f "$sdir/frosty.pid" ]; then
            printf "${yellow}warning: no PID file exists for server ${cyan}$name${yellow}. Trying to guess what to kill...${reset}\n"

            for pane_pid in $(server_tmux list-panes -a -F "#{pane_pid}"); do
                printf "${yellow}killing process group ${blue}${pane_pid}${yellow} with the following processes:${reset}\n"
                ps ax -o  "%p %r %y %x %c " | grep -i $pane_pid
                kill $2 -- -"$pane_pid"
            done
        else
            while read -r killpid; do
                printf "${yellow}killing PID $blue${killpid}${yellow}...${reset}\n"
                kill $2 "$killpid"
            done
        fi
        ;;
    restart)
        "$0" stop "$name"
        printf "${green}waiting for ${cyan}${name}${green} to stop ${reset}(push Ctrl+C to cancel)..."
        while server_status "$name"; do
            sleep 1
            printf "."
        done
        printf "\n"
        "$0" start "$name"
        ;;
    status)
        server_status "$name"
        status_ret=$?
        case "$status_ret" in
            0)
                printf "${green}running${reset}\n"
                exit 0
                ;;
            1)
                printf "${red}not running${reset}\n"
                exit 1
                ;;
            *)
                printf "unauthorized\n"
                exit 2
                ;;
        esac
        ;;
    console)
        if [ $# -gt 0 ]; then
            server_command "$@"
        else
            server_tmux attach-session -t "$name"
        fi
        ;;
    archive)
        case "$scmd" in
            create)
                (
                cd "$sdir"
                mkdir -p "$archive_dir/$name"
                archive_path="$archive_dir/$name/${sprefix}-${name}_$(date '+%F_%H_%M_%S').tar.gz"
                printf "${green}archiving ${cyan}$name${green} to ${blue}$archive_path${reset}\n"
                tar --totals -czf "$archive_path" * || { printf "${red}error: archive failed!${reset}\n" && exit 1; }
                printf "${green}archive created at ${blue}$archive_path${reset}\n"
                ) || exit 1
                ;;
            list)
                ls --color -lh "$archive_dir/$name" || { printf "${cyan}${name}${reset} has no archives yet! Try ${cyan}frosty ${green}archive ${cyan}$name ${blue}create${reset}!\n" && exit 1; }
                printf "${green}(you can manage archives through your file manager/FTP client)${reset}\n"
                ;;
            restore)
                printf "${green}select an archive file to restore:${reset}\n\n"
                declare -a archive_list
                i=0
                for f in $(ls "$archive_dir/$name"); do
                    archive_list[$i]="$f"
                    i=$(($i+1))
                    printf "${blue}$i\t${green}${archive_list[$(($i-1))]}${reset}\n"
                done
                printf "${green}type the number of the archive you want to restore, or Ctrl+C to cancel: ${blue}"
                read archive_index
                archive_index="$(($archive_index - 1))"
                if [ $archive_index -lt 0 ] || [ $archive_index -ge "${#archive_list[@]}" ]; then
                    printf "${red}error: you need to enter one of the numbers that were presented to you in the menu${reset}\n"
                    exit 1
                fi
                archive_to_restore="$archive_dir/$name/${archive_list[$archive_index]}"

                printf "${yellow}ARE YOU SURE you want to RESTORE server ${cyan}$name${yellow} from the archive file ${blue}${archive_to_restore}${yellow}? Type ${green}y${yellow} for yes or ${red}n${yellow} for no.${reset} "
                read yn
                case "$yn" in
                    y)
                        printf "${yellow}restoring ${cyan}${name}${yellow} from ${blue}${archive_to_restore}${yellow}... ${reset}\n"
                        rm -rf "$sdir/*"
                        cd "$sdir"
                        tar --totals -xzf "${archive_to_restore}"
                        printf "${yellow}done restoring${reset}\n"
                        ;;
                    *)
                        printf "${white}archive not restored${reset}\n"
                        exit 1
                        ;;
                esac
                exit 0
                ;;
            purge)
                if [ $# -lt 1 ]; then
                    printf "${red}error: please specify how many days of archives you wish to keep!${reset}\n"
                    exit 1
                fi

                keep_days=$(($1))
                shift

                if [ "$keep_days" -lt 1 ]; then
                    printf "${red}error: please use a number greater than 0 for the number of days to keep!${reset}\n"
                    exit 1
                fi

                printf "${yellow}deleting archives...\n"
                find "$archive_dir/$name" -name "$sprefix"'-*' -mtime "+$keep_days" -delete -print
                printf "${reset}"
                ;;
            *)
                printf "${red}error: unknown archive command${reset}\n"
                exit 1
                ;;
        esac
        ;;
    backup)
        case "$scmd" in
            create)
                rdiff-backup "$sdir" "$backup_dir/$name"
                ;;
            restore)
                [ $# -lt 1 ] && echo "specify the amount of time ago, e.g. 10D" && exit 1
                server_status "$name" && echo "error: server must not be running" && exit 1
                read -p "warning: this will overwrite live files in '$name'. ok? [yn] " yn
                [ "$yn" != "y" ] && echo "not overwriting..." && exit 1
                rdiff-backup -r "$1" --force "$backup_dir/$name" "$sdir"
                ;;
            remove)
                [ $# -lt 1 ] && echo "specify the amount of time ago, e.g. 10D" && exit 1
                rdiff-backup --remove-older-than "$1" "$backup_dir/$name"
                ;;
            list)
                rdiff-backup -l "$backup_dir/$name"
                ;;
            *)
                echo "error: unknown backup command"
                exit 1
                ;;
        esac
        ;;
    path)
        echo "$sdir"
        ;;
    fix)
        chmod -R u+rw,g+rw "$sdir"/*
        find "$sdir" -type d -exec chmod u+rwx,g+rwx "{}" \;
        ;;
    *)
        echo "${red}error: invalid command${reset}\n"
        usage
        exit 1
        ;;
esac

