#!/bin/bash
dest_bin="/usr/local/bin"
dest_conf="/etc/frosty.conf"

[ $(id -u) != 0 ] && echo "error: you must run this as root" && exit 1

# compile
gcc -Og -g -o frosty wrapper.c

cp frosty.conf "$dest_conf"
chmod 644 "$dest_conf"

cp frosty frosty.sh "$dest_bin"

chown root:root "$dest_bin/"{frosty,frosty.sh} "$dest_conf"
# setuid root
chmod 4755 "$dest_bin/frosty"
chmod 755 "$dest_bin/frosty.sh"

# make frosty dir
mkdir -p /var/frosty
cp log4j2.xml tmux.conf /var/frosty
chown frosty:frosty /var/frosty

